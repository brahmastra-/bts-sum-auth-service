package com.sum.auth.service.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.sum.auth.service.dto.OutputUserDetailsDto;
import com.sum.auth.service.model.TbUserMaster;

@Component
public class UserMgmtMapper {

	public static OutputUserDetailsDto mapUserDetails(TbUserMaster tbUserMaster) {
		OutputUserDetailsDto outputUserDetailsDto=new OutputUserDetailsDto();
		
		outputUserDetailsDto.setnUserId(tbUserMaster.getnUserId());
		
		outputUserDetailsDto.setsFirstName(tbUserMaster.getsFirstName());
		outputUserDetailsDto.setsMiddleName(tbUserMaster.getsMiddleName());
		outputUserDetailsDto.setsGender(tbUserMaster.getsGender());
		outputUserDetailsDto.setsEmailId(tbUserMaster.getsEmailId());
		outputUserDetailsDto.setsMobileNumber(tbUserMaster.getsMobileNumber());
		
		
		// TODO Auto-generated method stub
		return outputUserDetailsDto;
	}

	public static OutputUserDetailsDto mapAllUserDetails(TbUserMaster tbUserMaster) {
OutputUserDetailsDto outputUserDetailsDto=new OutputUserDetailsDto();
		
		outputUserDetailsDto.setnUserId(tbUserMaster.getnUserId());
		
		outputUserDetailsDto.setsFirstName(tbUserMaster.getsFirstName());
		outputUserDetailsDto.setsMiddleName(tbUserMaster.getsMiddleName());
		outputUserDetailsDto.setsGender(tbUserMaster.getsGender());
		outputUserDetailsDto.setsEmailId(tbUserMaster.getsEmailId());
		outputUserDetailsDto.setsMobileNumber(tbUserMaster.getsMobileNumber());
		
		
		// TODO Auto-generated method stub
		return outputUserDetailsDto;
	}

	
	

}
