package com.sum.auth.service.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.pl.NIP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sum.auth.service.dto.InputUserDetailsDto;
import com.sum.auth.service.dto.OutputResponseDto;
import com.sum.auth.service.dto.ValidationMessageDto;
import com.sum.auth.service.exception.ValidationException;
import com.sum.auth.service.messages.Messages;
import com.sum.auth.service.service.IUserMgmtService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin
@RestController
public class UserRegistrationController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IUserMgmtService iUserMgmtService;
	@Autowired
	private Messages messages;

	/**
	 * Registration user
	 * 
	 * @param input         all the user informations
	 * @param bindingResult
	 * @return
	 */
	@ApiOperation(value = "Add user information")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "success"),
			@ApiResponse(code = 404, message = "Not resgister user information") })

	@RequestMapping(value = "/userRegistration", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<OutputResponseDto> registrstionUser(
			@RequestBody InputUserDetailsDto inputUserDetailsDto, final BindingResult bindingResult) {
		logger.info("inputUserDetaislsDto:{}", inputUserDetailsDto.toString());
		if (bindingResult.hasErrors()) {  

			//Object o1 = bindingResult.getAllErrors();
			//ValidationMessageDto v = new ArrayList<>();
			ValidationMessageDto e = new ValidationMessageDto();
			for (FieldError err :bindingResult.getFieldErrors())
			{
				
				e.setErrorMessage(err.getDefaultMessage());
				e.setField(err.getField());
				logger.error("Error in {} | {}", err.getField(), err.getDefaultMessage());
				e.setErrorMessage(err.getDefaultMessage());

			}

			//int i = bindingResult.getErrorCount();
			throw new ValidationException("RegistrationRequest",
					false ,
					messages.getStastusCode("common.validation.exception.invalidParameters.id"),
					"Validation",
					e.getErrorMessage());  
		}
		return getResponseEntity(iUserMgmtService.registerUserDetails(inputUserDetailsDto));
	}

	@RequestMapping(value = "/getUserdetails", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<OutputResponseDto> getAllUserDetails(@RequestParam("id") Long nUserId) {

		return getResponseEntity(iUserMgmtService.getAllUserDetails(nUserId));

	}

	public ResponseEntity<OutputResponseDto> getResponseEntity(OutputResponseDto outputResponseDto) {
		HttpStatus st = null;
		String sCode = "";
		String sAppCode = "-";
		try {
			sAppCode = outputResponseDto.getStatusCode().toString();
			sCode = sAppCode.split("-")[1].toString();
			st = HttpStatus.valueOf(Integer.parseInt(sCode));

		} catch (Exception e) {
			st = HttpStatus.I_AM_A_TEAPOT;
			logger.error("Incorrect Http Header set | sAppCode:{} | HttpCode:{}" + sAppCode, sCode);
		}
		return new ResponseEntity<OutputResponseDto>(outputResponseDto, st);
	}
}
