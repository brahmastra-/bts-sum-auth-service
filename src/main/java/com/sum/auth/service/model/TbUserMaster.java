package com.sum.auth.service.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Author:- Saudagar chaudahary
 * 
 * The persistent class for the tb_user_master database table.
 * 
 */
@Entity
@Table(name = "tb_user_master")
@NamedQuery(name = "TbUserMaster.findAll", query = "SELECT t FROM TbUserMaster t")
public class TbUserMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long Id;
	@Column(name = "n_userid")
	private Long nUserId;
	@Column(name = "d_created_on")
	private Timestamp dCreatedOn;

	@Column(name = "d_modified_on")
	private Timestamp dModifiedOn;

	@Column(name = "n_created_by")
	private Long nCreatedBy;

	@Column(name = "n_modified_by")
	private Long nModifiedBy;

	@Column(name = "s_email_id")
	private String sEmailId;
	@Column(name = "s_first_name")
	private String sFirstName;

	@Column(name = "s_gender")
	private String sGender;

	@Column(name = "s_last_name")
	private String sLastName;

	@Column(name = "s_middle_name")
	private String sMiddleName;

	@Column(name = "s_otp")
	private String sOtp;

	@Column(name = "s_password")
	private String sPassword;
	
	@Column(name = "s_mobile_number")
	private String sMobileNumber;
	@Column(name ="s_pin_number")
	private String sPinNumber;
	
	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getsPinNumber() {
		return sPinNumber;
	}

	public void setsPinNumber(String sPinNumber) {
		this.sPinNumber = sPinNumber;
	}

	public String getsMobileNumber() {
		return sMobileNumber;
	}

	public void setsMobileNumber(String sMobileNumber) {
		this.sMobileNumber = sMobileNumber;
	}

	public TbUserMaster() {
	}

	public Long getnUserId() {
		return nUserId;
	}

	public void setnUserId(Long nUserId) {
		this.nUserId = nUserId;
	}

	public Timestamp getdCreatedOn() {
		return dCreatedOn;
	}

	public void setdCreatedOn(Timestamp dCreatedOn) {
		this.dCreatedOn = dCreatedOn;
	}

	public Timestamp getdModifiedOn() {
		return dModifiedOn;
	}

	public void setdModifiedOn(Timestamp dModifiedOn) {
		this.dModifiedOn = dModifiedOn;
	}

	public Long getnCreatedBy() {
		return nCreatedBy;
	}

	public void setnCreatedBy(Long nCreatedBy) {
		this.nCreatedBy = nCreatedBy;
	}

	public Long getnModifiedBy() {
		return nModifiedBy;
	}

	public void setnModifiedBy(Long nModifiedBy) {
		this.nModifiedBy = nModifiedBy;
	}

	public String getsEmailId() {
		return sEmailId;
	}

	public void setsEmailId(String sEmailId) {
		this.sEmailId = sEmailId;
	}

	public String getsFirstName() {
		return sFirstName;
	}

	public void setsFirstName(String sFirstName) {
		this.sFirstName = sFirstName;
	}

	public String getsGender() {
		return sGender;
	}

	public void setsGender(String sGender) {
		this.sGender = sGender;
	}

	public String getsLastName() {
		return sLastName;
	}

	public void setsLastName(String sLastName) {
		this.sLastName = sLastName;
	}

	public String getsMiddleName() {
		return sMiddleName;
	}

	public void setsMiddleName(String sMiddleName) {
		this.sMiddleName = sMiddleName;
	}

	public String getsOtp() {
		return sOtp;
	}

	public void setsOtp(String sOtp) {
		this.sOtp = sOtp;
	}

	public String getsPassword() {
		return sPassword;
	}

	public void setsPassword(String sPassword) {
		this.sPassword = sPassword;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}