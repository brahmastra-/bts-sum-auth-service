package com.sum.auth.service.repository;

import org.springframework.data.repository.CrudRepository;

import com.sum.auth.service.model.TbUserMaster;

public interface IUserMasterRepository extends CrudRepository<TbUserMaster, Long>{


	TbUserMaster findBynUserId(Long nUserId);
	
	
	
}
