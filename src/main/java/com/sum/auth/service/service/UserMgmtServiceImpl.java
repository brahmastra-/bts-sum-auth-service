package com.sum.auth.service.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sum.auth.service.dto.InputUserDetailsDto;
import com.sum.auth.service.dto.OutputResponseDto;
import com.sum.auth.service.dto.OutputUserDetailsDto;
import com.sum.auth.service.mapper.UserMgmtMapper;
import com.sum.auth.service.messages.Messages;
import com.sum.auth.service.model.TbUserMaster;
import com.sum.auth.service.repository.IUserMasterRepository;

@Service
public class UserMgmtServiceImpl implements IUserMgmtService {
	@Autowired
	IUserMasterRepository iUserMasterRepository;
	@Autowired
	UserMgmtMapper userMgmtMapper;

	@Autowired
	private Messages messages;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public OutputResponseDto getAllUserDetails(Long nUserId) {
		logger.info("Get details of users");
		List<OutputUserDetailsDto> outputUserDetailsDtoList = new ArrayList<OutputUserDetailsDto>();
		if (nUserId == null) {
			List<TbUserMaster> tbUserMaster = (List<TbUserMaster>) iUserMasterRepository.findAll();
			for (TbUserMaster usetDetails : tbUserMaster) {
				OutputUserDetailsDto outputUserDetailsDto = UserMgmtMapper.mapAllUserDetails(usetDetails);
				outputUserDetailsDtoList.add(outputUserDetailsDto);

			}
		} else {
			TbUserMaster tbUserMaster = iUserMasterRepository.findBynUserId(nUserId);
			OutputUserDetailsDto outputUserDetailsDto = UserMgmtMapper.mapAllUserDetails(tbUserMaster);
			outputUserDetailsDtoList.add(outputUserDetailsDto);

		}

		return new OutputResponseDto(true, outputUserDetailsDtoList, messages.get("user.feath.success.id"),
				messages.get("user.fetch.message.success"), null);
	}

	@Override
	public OutputResponseDto registerUserDetails( InputUserDetailsDto inputUserDetailsDto) {
		TbUserMaster tbUserMaster=new TbUserMaster();
		Timestamp timestamp=new Timestamp(System.currentTimeMillis());
		tbUserMaster.setdCreatedOn(timestamp);
		tbUserMaster.setdModifiedOn(timestamp);
		tbUserMaster.setnCreatedBy(inputUserDetailsDto.getnCreatedBy());
		tbUserMaster.setnUserId(inputUserDetailsDto.getnUserId());
		tbUserMaster.setsEmailId(inputUserDetailsDto.getsEmailId());
		tbUserMaster.setsFirstName(inputUserDetailsDto.getsFirstName());
		tbUserMaster.setsLastName(inputUserDetailsDto.getsLastName());
		tbUserMaster.setsGender(inputUserDetailsDto.getsGender());
		tbUserMaster.setsMiddleName(inputUserDetailsDto.getsMiddleName());
		tbUserMaster.setsMobileNumber(inputUserDetailsDto.getsMobileNumber());
		tbUserMaster.setsPinNumber(inputUserDetailsDto.getsPinNumber());
		iUserMasterRepository.save(tbUserMaster);
		
		return new OutputResponseDto(true, null, messages.get("user.save.success.id"),
				messages.get("user.save.success.message"), null);
	}

}
