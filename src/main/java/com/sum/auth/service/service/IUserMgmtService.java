package com.sum.auth.service.service;

import org.springframework.stereotype.Service;

import com.sum.auth.service.dto.InputUserDetailsDto;
import com.sum.auth.service.dto.OutputResponseDto;


@Service
public interface IUserMgmtService {
	public OutputResponseDto getAllUserDetails(Long nUserId);

	public OutputResponseDto registerUserDetails(InputUserDetailsDto inputUserDetailsDto);


}
