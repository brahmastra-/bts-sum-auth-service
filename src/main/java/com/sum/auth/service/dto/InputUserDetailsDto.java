package com.sum.auth.service.dto;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class InputUserDetailsDto {
	//@NotNull(message="User Id should not be empty.")
	private Long nUserId;
	//@NotBlank(message="This field should not be blank.")
	
	private Timestamp dCreatedOn;

	private Timestamp dModifiedOn;

	private Long nCreatedBy;
	private Long nModifiedBy;
	@NotNull(message="E-Mail should not be empty.")
	//@Pattern(message="Please Enter Valid E-Mail Id",regexp="^[0-9]*$")
	private String sEmailId;
	@NotNull(message="Frist Name should not be empty.")
	private String sFirstName;
	@NotNull(message="sGender should not be empty.")
	private String sGender;
	@NotNull(message="Last Name should not be empty.")
	private String sLastName;
	
	private String sMiddleName;

	private String sOtp;

	private String sPassword;
	@NotNull(message="Mobile Number should not be empty")
	private String sMobileNumber;
	private String sPinNumber;

	public String getsPinNumber() {
		return sPinNumber;
	}

	public void setsPinNumber(String sPinNumber) {
		this.sPinNumber = sPinNumber;
	}

	public String getsMobileNumber() {
		return sMobileNumber;
	}

	public void setsMobileNumber(String sMobileNumber) {
		this.sMobileNumber = sMobileNumber;
	}

	public Long getnUserId() {
		return nUserId;
	}

	public void setnUserId(Long nUserId) {
		this.nUserId = nUserId;
	}

	public Timestamp getdCreatedOn() {
		return dCreatedOn;
	}

	public Timestamp getdModifiedOn() {
		return dModifiedOn;
	}

	public void setdModifiedOn(Timestamp dModifiedOn) {
		this.dModifiedOn = dModifiedOn;
	}

	public void setdCreatedOn(Timestamp dCreatedOn) {
		this.dCreatedOn = dCreatedOn;
	}

	public Long getnCreatedBy() {
		return nCreatedBy;
	}

	public void setnCreatedBy(Long nCreatedBy) {
		this.nCreatedBy = nCreatedBy;
	}

	public Long getnModifiedBy() {
		return nModifiedBy;
	}

	public void setnModifiedBy(Long nModifiedBy) {
		this.nModifiedBy = nModifiedBy;
	}

	public String getsEmailId() {
		return sEmailId;
	}

	public void setsEmailId(String sEmailId) {
		this.sEmailId = sEmailId;
	}

	public String getsFirstName() {
		return sFirstName;
	}

	public void setsFirstName(String sFirstName) {
		this.sFirstName = sFirstName;
	}

	public String getsGender() {
		return sGender;
	}

	public void setsGender(String sGender) {
		this.sGender = sGender;
	}

	public String getsLastName() {
		return sLastName;
	}

	public void setsLastName(String sLastName) {
		this.sLastName = sLastName;
	}

	public String getsMiddleName() {
		return sMiddleName;
	}

	public void setsMiddleName(String sMiddleName) {
		this.sMiddleName = sMiddleName;
	}

	public String getsOtp() {
		return sOtp;
	}

	public void setsOtp(String sOtp) {
		this.sOtp = sOtp;
	}

	public String getsPassword() {
		return sPassword;
	}

	public void setsPassword(String sPassword) {
		this.sPassword = sPassword;
	}

	@Override
	public String toString() {
		return "InputUserDetailsDto [nUserId=" + nUserId + ", dCreatedOn=" + dCreatedOn + ", dModifiedOn=" + dModifiedOn
				+ ", nCreatedBy=" + nCreatedBy + ", nModifiedBy=" + nModifiedBy + ", sEmailId=" + sEmailId
				+ ", sFirstName=" + sFirstName + ", sGender=" + sGender + ", sLastName=" + sLastName + ", sMiddleName="
				+ sMiddleName + ", sOtp=" + sOtp + ", sPassword=" + sPassword + ", sMobileNumber=" + sMobileNumber
				+ ", sPinNumber=" + sPinNumber + "]";
	}

}
