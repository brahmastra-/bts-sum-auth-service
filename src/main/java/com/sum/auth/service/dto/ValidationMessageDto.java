package com.sum.auth.service.dto;
public class ValidationMessageDto {
	
	
	@Override
	public String toString() {
		return "ValidationMessageDto [field=" + field + ", errorMessage=" + errorMessage + "]";
	}

	private String field;
	private String errorMessage;
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ValidationMessageDto(String field, String errorMessage) {
		super();
		this.field = field;
		this.errorMessage = errorMessage;
	}
	
	public ValidationMessageDto() {
		// TODO Auto-generated constructor stub
	}
}