package com.sum.auth.service.dto;

public class OutputUserDetailsDto {

	private Long nUserId;

	private String dCreatedOn;

	private String dModifiedOn;

	private Long nCreatedBy;
	private Long nModifiedBy;

	private String sEmailId;

	private String sFirstName;

	private String sGender;

	private String sLastName;

	private String sMiddleName;

	private String sOtp;

	private String sPassword;
	private String sMobileNumber;

	

	public String getsMobileNumber() {
		return sMobileNumber;
	}

	public void setsMobileNumber(String sMobileNumber) {
		this.sMobileNumber = sMobileNumber;
	}

	public Long getnUserId() {
		return nUserId;
	}

	public void setnUserId(Long nUserId) {
		this.nUserId = nUserId;
	}

	public String getdCreatedOn() {
		return dCreatedOn;
	}

	public void setdCreatedOn(String dCreatedOn) {
		this.dCreatedOn = dCreatedOn;
	}

	public String getdModifiedOn() {
		return dModifiedOn;
	}

	public void setdModifiedOn(String dModifiedOn) {
		this.dModifiedOn = dModifiedOn;
	}

	public Long getnCreatedBy() {
		return nCreatedBy;
	}

	public void setnCreatedBy(Long nCreatedBy) {
		this.nCreatedBy = nCreatedBy;
	}

	public Long getnModifiedBy() {
		return nModifiedBy;
	}

	public void setnModifiedBy(Long nModifiedBy) {
		this.nModifiedBy = nModifiedBy;
	}

	public String getsEmailId() {
		return sEmailId;
	}

	public void setsEmailId(String sEmailId) {
		this.sEmailId = sEmailId;
	}

	public String getsFirstName() {
		return sFirstName;
	}

	public void setsFirstName(String sFirstName) {
		this.sFirstName = sFirstName;
	}

	public String getsGender() {
		return sGender;
	}

	public void setsGender(String sGender) {
		this.sGender = sGender;
	}

	public String getsLastName() {
		return sLastName;
	}

	public void setsLastName(String sLastName) {
		this.sLastName = sLastName;
	}

	public String getsMiddleName() {
		return sMiddleName;
	}

	public void setsMiddleName(String sMiddleName) {
		this.sMiddleName = sMiddleName;
	}

	public String getsOtp() {
		return sOtp;
	}

	public void setsOtp(String sOtp) {
		this.sOtp = sOtp;
	}

	public String getsPassword() {
		return sPassword;
	}

	public void setsPassword(String sPassword) {
		this.sPassword = sPassword;
	}

	
	@Override
	public String toString() {
		return "OutputUserDetailsDto [nUserId=" + nUserId + ", dCreatedOn=" + dCreatedOn + ", dModifiedOn="
				+ dModifiedOn + ", nCreatedBy=" + nCreatedBy + ", nModifiedBy=" + nModifiedBy + ", sEmailId=" + sEmailId
				+ ", sFirstName=" + sFirstName + ", sGender=" + sGender + ", sLastName=" + sLastName + ", sMiddleName="
				+ sMiddleName + ", sOtp=" + sOtp + ", sPassword=" + sPassword + ", sMobileNumber=" + sMobileNumber
				+ "]";
	}
	
}
