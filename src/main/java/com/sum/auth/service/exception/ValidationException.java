
package com.sum.auth.service.exception;

import org.springframework.http.HttpStatus;

public class ValidationException extends BaseException {
	private static final long serialVersionUID = 1L;
	// http status code
	private HttpStatus httpstatus;
	private Object o;

	public Object getO() {
		return o;
	}

	public void setO(Object o) {
		this.o = o;
	}

	public HttpStatus getHttpstatus() {
		return httpstatus;
	}

	public void setHttpstatus(HttpStatus httpstatus) {
		this.httpstatus = httpstatus;
	}

	public ValidationException(HttpStatus httpstatus) {
		super();
		this.httpstatus = httpstatus;
	}

	public ValidationException(String type, boolean status, String statuscode, String message, Object o1) {
		super(type, status, statuscode, message);
		this.o = o1;
		this.httpstatus = HttpStatus.BAD_REQUEST;
		// TODO Auto-generated constructor stub
	}

	public ValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ValidationException() {

	}

}
